# make sure below variables can be overwritten so use `?=`
DEBUG ?= 1
UBSAN_ENABLED ?= false
RELEASE_BUILD ?= true
